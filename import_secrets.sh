#!/bin/bash

set -euo pipefail

. cki_utils.sh

if ! [ -v SECRETS_FILE ]; then
    SECRETS_FILE=secrets.yml
    export SECRETS_FILE
fi

SECRETS_FILE=$(readlink -f "${SECRETS_FILE}")
export CACHE_DIR=${1:-}
SECRETS_DIR="$(mktemp -d)"
export SECRETS_DIR
trap 'rm -rf "${SECRETS_DIR}"' EXIT

function read_secrets() {
    declare -xgA SECRETS=()
    while IFS= read -r -d '' key; do
        IFS= read -r -d '' value
        SECRETS[$key]=${value%$'\n'}
    done < <(shyaml key-values-0 < "$SECRETS_FILE")
    SECRETS[SECRETS_FILE]="${SECRETS_FILE}"
}

function decode_var {
    read_secrets
    for KEY in "$@"; do
        VALUE=${SECRETS[$KEY]}
        # Look for OpenSSL 8-byte signature: "Salted__"
        # http://justsolve.archiveteam.org/wiki/OpenSSL_salted_format
        if [[ $VALUE == U2FsdGVkX1* ]]; then
            HASH=$(md5sum <<< "$VALUE" | cut -d ' ' -f 1)
            if [ -n "$CACHE_DIR" ] && [ -r "$CACHE_DIR/$HASH" ]; then
                VALUE=$(cat "$CACHE_DIR/$HASH")
            else
                VALUE=$(echo "$VALUE" | cki_openssl_enc ENVPASSWORD -d -a)
                if [ -n "$CACHE_DIR" ]; then
                    echo "$VALUE" > "$CACHE_DIR/$HASH"
                fi
            fi
        fi
        export $KEY="$VALUE"
        # add -g so declare does not make NAMEs local
        declare -p "${KEY}" | sed '1s/^declare -x/&g/' > "${SECRETS_DIR}/${KEY}"
    done
}

read_secrets
CPU_COUNT="$(nproc)"
CPU_COUNT=$(( CPU_COUNT > 8 ? 8 : CPU_COUNT ))  # limit to avoid OOM on huge k8s nodes
KEYS_PER_CPU="$((("${#SECRETS[@]}" + CPU_COUNT - 1) / CPU_COUNT))"

export -f decode_var read_secrets cki_openssl_enc
printf "%s\0" "${!SECRETS[@]}" | xargs -0 -n "${KEYS_PER_CPU}" -P "${CPU_COUNT}" bash -c 'decode_var "$@"' _
cat "${SECRETS_DIR}"/*
